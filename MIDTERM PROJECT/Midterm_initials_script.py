#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
##

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
# import gazebo/gazebo.hh
# import gazebo/physics/Joint.hh
# import gazebo/physics/JointController.hh
# import gazebo/physics/Model.hh
# import gazebo/physics/PhysicsTypes.hh

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class RobotController(object):
    """RobotController"""

    def __init__(self):
        super(RobotController, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## END_SUB_TUTORIAL

        ## BEGIN_SUB_TUTORIAL basic_info
        ##
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names
    
    def go_to_inital_pos(self):
        # This method is used to initialize the end-effector for an
        # initial position for the upcoming drawing method

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        
        joint_goal[0] = 0
        joint_goal[1] = 0
        joint_goal[2] = 0
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = 0
        
        joint_goal[0] = tau/1.8
        joint_goal[1] = -tau/2.25
        joint_goal[2] = -tau/8
        joint_goal[3] = -tau/1.07462
        joint_goal[4] = -tau/1.8
        joint_goal[5] = tau/2  # 1/6 of a turn
        
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        return 0
        
    def make_A(self):
        # This method is used to actually make the A.
        # Joint space is used to make the A.
        
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        
        joint_goal[0] = tau/1.8
        joint_goal[1] = -tau/2.25
        joint_goal[2] = -tau/8
        joint_goal[3] = -tau/1.07462
        joint_goal[4] = -tau/1.8
        joint_goal[5] = tau/2  # 1/6 of a turn

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (200/180) *pi
        joint_goal[1] = (-138/180) *pi
        joint_goal[2] = (-68/180) *pi
        joint_goal[3] = (-334/180) *pi
        joint_goal[4] = (-201/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn

        move_group.go(joint_goal, wait=True)

        # TOP OF A
        joint_goal[0] = (200/180) *pi
        joint_goal[1] = (-111/180) *pi
        joint_goal[2] = (-82/180) *pi
        joint_goal[3] = (-346/180) *pi
        joint_goal[4] = (-204/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (205/180) *pi
        joint_goal[1] = (-111/180) *pi
        joint_goal[2] = (-116/180) *pi
        joint_goal[3] = (-312/180) *pi
        joint_goal[4] = (-209/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn
        

        move_group.go(joint_goal, wait=True)

        #bottom right of A
        joint_goal[0] = (211/180) *pi
        joint_goal[1] = (-120/180) *pi
        joint_goal[2] = (-132/180) *pi
        joint_goal[3] = (-287/180) *pi
        joint_goal[4] = (-215/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (205/180) *pi
        joint_goal[1] = (-111/180) *pi
        joint_goal[2] = (-116/180) *pi
        joint_goal[3] = (-312/180) *pi
        joint_goal[4] = (-209/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn

        # # The go command can be called with joint values, poses, or without any
        # # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        #final line
        joint_goal[0] = (196/180) *pi
        joint_goal[1] = (-132/180) *pi
        joint_goal[2] = (-78/180) *pi
        joint_goal[3] = (-328/180) *pi
        joint_goal[4] = (-200/180) *pi
        joint_goal[5] = (180/180) *pi  # 1/6 of a turn

        # # The go command can be called with joint values, poses, or without any
        # # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        ## END_SUB_TUTORIAL

        return 0
    
    
    def go_to_starting_J(self):
        # This method is used to move the end-effector to the 
        # starting position for the initial J
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        
        # TOP LEFT OF J 
        joint_goal[0] = (196/180) *pi
        joint_goal[1] = (-123/180) *pi
        joint_goal[2] = (-59/180) *pi
        joint_goal[3] = (-356/180) *pi
        joint_goal[4] = (-200/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)
        
        move_group.stop()


        return 0        




    def make_J(self):
        # This method is used to actually make the J.
        # Joint space is used to make the J.
        
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        
        joint_goal[0] = (207/180) *pi
        joint_goal[1] = (-91/180) *pi
        joint_goal[2] = (-102/180) *pi
        joint_goal[3] = (-345/180) *pi
        joint_goal[4] = (-211/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)
        
        #TOP RIGHT OF J
        joint_goal[0] = (234/180) *pi
        joint_goal[1] = (-63/180) *pi
        joint_goal[2] = (-124/180) *pi
        joint_goal[3] = (-352/180) *pi
        joint_goal[4] = (-238/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (207/180) *pi
        joint_goal[1] = (-91/180) *pi
        joint_goal[2] = (-102/180) *pi
        joint_goal[3] = (-345/180) *pi
        joint_goal[4] = (-211/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        #DOWN FOR J
        joint_goal[0] = (207/180) *pi
        joint_goal[1] = (-105/180) *pi
        joint_goal[2] = (-118/180) *pi
        joint_goal[3] = (-315/180) *pi
        joint_goal[4] = (-211/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        #DOWN-END FOR J
        joint_goal[0] = (207/180) *pi
        joint_goal[1] = (-123/180) *pi
        joint_goal[2] = (-121/180) *pi
        joint_goal[3] = (-294/180) *pi
        joint_goal[4] = (-211/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        #ENDING CURVE
        joint_goal[0] = (205/180) *pi
        joint_goal[1] = (-128/180) *pi
        joint_goal[2] = (-88/180) *pi
        joint_goal[3] = (-322/180) *pi
        joint_goal[4] = (-209/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)



        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        ## END_SUB_TUTORIAL

        return 0

    def go_to_starting_M(self):
        # This method is used to move the end-effector to the 
        # starting position for the initial M
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        
        # BOTTOM LEFT OF M
        joint_goal[0] = (193/180) *pi
        joint_goal[1] = (-160/180) *pi
        joint_goal[2] = (-44/180) *pi
        joint_goal[3] = (-332/180) *pi
        joint_goal[4] = (-197/180) *pi
        joint_goal[5] = (-180/180) *pi

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        ## END_SUB_TUTORIAL

        return 0


    def make_M(self):
        # This method is used to actually make the M.
        # Joint space is used to make the M.
        
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        joint_goal[0] = (199/180) *pi
        joint_goal[1] = (-116/180) *pi
        joint_goal[2] = (-71/180) *pi
        joint_goal[3] = (-349/180) *pi
        joint_goal[4] = (-203/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (204/180) *pi
        joint_goal[1] = (-110/180) *pi
        joint_goal[2] = (-104/180) *pi
        joint_goal[3] = (-323/180) *pi
        joint_goal[4] = (-208/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        joint_goal[0] = (214/180) *pi
        joint_goal[1] = (-86/180) *pi
        joint_goal[2] = (-109/180) *pi
        joint_goal[3] = (-343/180) *pi
        joint_goal[4] = (-218/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)


        joint_goal[0] = (242/180) *pi
        joint_goal[1] = (-126/180) *pi
        joint_goal[2] = (-150/180) *pi
        joint_goal[3] = (-262/180) *pi
        joint_goal[4] = (-246/180) *pi
        joint_goal[5] = (-180/180) *pi 

        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        ## END_SUB_TUTORIAL

        return 0

def main():
    try:
        input(
            "============ Press `Enter` to begin the tutorial by setting up the moveit_commander ..."
        )
        tutorial = RobotController()

        input(
            "============ Press `Enter` to initalize UR5e end-effector"
        )
        tutorial.go_to_inital_pos()

        input(
            "============ Press `Enter` to make the letter A"
        )
        tutorial.make_A()

        input(
            "============ Press `Enter` to move to starting position for second letter"
        )
        tutorial.go_to_starting_J()

        input(
            "============ Press `Enter` to make the letter J"
        )
        tutorial.make_J()

        input(
            "============ Press `Enter` to move to starting position for third letter"
        )
        tutorial.go_to_starting_M()

        input(
            "============ Press `Enter` to make the letter M"
        )
        tutorial.make_M()

        print("============ Demo complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()