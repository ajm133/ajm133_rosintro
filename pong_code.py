# Modified code from https://github.com/ros-planning/moveit_tutorials/blob/master/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py
from __future__ import print_function
from six.moves import input

import sys
import rospy
import moveit_commander

from gazebo_msgs.srv import SpawnModel
import rospy
from geometry_msgs.msg import Pose

from math import pi

class MoveGroupPythonInterface(object):
    """MoveGroupPythonInterface"""

    def __init__(self):
        super(MoveGroupPythonInterface, self).__init__()

        #initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface", anonymous=True)

        # Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        # kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        # Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        # for getting, setting, and updating the robot's internal understanding of the
        # surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a `MoveGroupCommander`_ object.  This object is an interface to a planning group (group of joints).
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Getting Basic Information
        # get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # print the entire state of the robot for debugging:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        #set joint speeds here

        # Misc variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def ten_shots(self, j_goals):
        # Copy class variables to local variables
        move_group = self.move_group
        n = len(j_goals)
        # goals for
        j_goals = [[210, -121,0], [198, -101,0], [206, -110,0],
                            [229, -53,0], [207, -104,0], [270, -90,0],[270, -90,0],[270, -90,0],[270, -90,0],[270, -90,0]]
        for i in range(0,n):
            #reset cannon
            joint_goal = move_group.get_current_joint_values()
            joint_goal[0] = 0
            joint_goal[1] = 0
            joint_goal[2] = 0

            # spawn the pingpong ball in the cannon
            spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
            spawn_model_client(
                model_name='ground_plane',
                model_xml=open('/usr/share/gazebo-9/models/ground_plane/model.sdf', 'r').read(),
                robot_namespace='/foo',
                initial_pose=Pose(),
                reference_frame='world'
            )
            # set joint goals for robot joints (in radians)
            joint_goal = move_group.get_current_joint_values()
            joint_goal[0] = j_goals[i][0]*pi/180
            joint_goal[1] = j_goals[i][1]*pi/180

            #go to joint goal
            move_group.go(joint_goal, wait=True)

            #shoot the ball here before going to the next joint goal
            shoot_goal = [0,0,5]*pi/180
            move_group.go(shoot_goal, wait=True)

        # ensure that there is no residual movement
        move_group.stop()

def main():
    try:
        print("Let's play pong!")
        input(
            "============ Press `Enter` to set up the moveit_commander ..."
        )
        ur5_command = MoveGroupPythonInterface()

        input(
            "============ Press `Enter` to make 10 shots ..."
        )
        ur5_command.ten_shots()

        print("10 shots complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()

